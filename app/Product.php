<?php

namespace App;

use App\Brand;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public function brand(){
      return $this->belongsTo(Brand::class,"brand_id");
    }

    public function category(){
      return $this->belongsToMany(Category::class);
    }
}
