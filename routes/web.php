<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::resource('products', 'ProductController');
Route::get('/products/{product}/delete', 'ProductController@destroy');
Route::resource('brands', 'BrandController');
Route::get('/brands/{brand}/delete', 'BrandController@destroy');
Route::resource('categories', 'CategoryController');
Route::get('/categories/{category}/delete', 'CategoryController@destroy');
