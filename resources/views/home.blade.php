
@extends('layouts.navbar')
@section('content')
<div class="container">
  <div>
      @if(\Session::has('success'))
          <div class='alert alert-success'>
              <p><{{\Session::get('success')}}></p>
          </div><br>
      @endif


      <a href="products/create" class='btn btn-primary'>Insert product</a>
      <table class="table table-striped">
          <thead>
              <tr>
                  <th>No.</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Price (RM)</th>
                  <th>Brand</th>
                  <th>Category</th>
                  <th colspan="2">Action</th>
              </tr>
          </thead>
          <tbody>
              @php
                  $i=1;
              @endphp
              @foreach($products as $product)
                  <tr>
                      <td>@php echo $i++; @endphp</td>
                      <td>{{$product['code']}}</td>
                      <td>{{$product['name']}}</td>
                      <td>{{$product['price']}}</td>
                      <td>{{$product->brands->name }}</td>
                      <td>
                          @foreach($product->categories as $c)
                              {{$c->name}} <br>
                          @endforeach
                      </td>
                      <td>
                          <a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-warning">Edit</a>&nbsp;
                          <a href="{{action('ProductController@delete', $product['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>&nbsp;
                      </td>
                  <!--
                  <td>
                  <form action="{{action('ProductController@destroy', $product['id'])}}"
                  method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit" onclick="return
                  confirm('Are you sure?')">Delete</button>
                  </form>
                  </td>
                  -->
              </tr>
              @endforeach



          </tbody>
      </table>
  </div>
</div>
@endsection
