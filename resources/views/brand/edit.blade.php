@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Edit Brand Information</h2><br />
            <form method="post" action="{{action('BrandController@update', $brand->id)}}">
            @csrf
                <input name="_method" type="hidden" value="PATCH">
                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{$brand->name}}">
                        </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="company">Company:</label>
                                <input type="text" class="form-control" name="company" value="{{$brand->company}}">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4"></div>
                            <div class="form-group col-md-4" style="margin-top:60px">
                                <button type="submit" class="btn btn-success" style="marginleft:38px">Update</button>
                            </div>
                            </div>
            </form>
    </div>

@endsection
