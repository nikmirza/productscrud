@extends('layouts.app')

@section('content')

    <div>
        @if(\Session::has('success'))
            <div class='alert alert-success'>
                <p><{{\Session::get('success')}}></p>
            </div><br>
        @endif


        <a href="{{action('BrandController@create')}}" class='btn btn-primary'>Insert Brand</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=1;
                @endphp
                @foreach($brands as $brand)
                    <tr>
                        <td>@php echo $i++; @endphp</td>
                        <td>{{$brand['name']}}</td>
                        <td>{{$brand['company']}}</td>

                        <td>
                            <a href="{{action('BrandController@edit', $brand['id'])}}" class="btn btn-warning">Edit</a>&nbsp;
                            <a href="brands/{{$brand['id']}}/delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>&nbsp;
                        </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
@endsection
