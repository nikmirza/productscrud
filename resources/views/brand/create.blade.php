@extends('layouts.app')

@section('content')

    <div class="container">
            <h2>Add Brand</h2><br/>
            <form method="post" action="{{action('BrandController@store')}}" enctype="multipart/formdata">
                @csrf

               <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="name">Name: </label>
                        <input type="text" class="form-control" name="name" placeholder="e.g. Nestle">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="company">Company: </label>
                        <input type="text" class="form-control" name="company" placeholder="e.g. Nestle Sdn Bhd">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4" style="margin-top:60px">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </form>
    </div>

@endsection
