@extends('layouts.app')

@section('content')

    <div>
        @if(\Session::has('success'))
            <div class='alert alert-success'>
                <p><{{\Session::get('success')}}></p>
            </div><br>
        @endif


        <a href="{{action('CategoryController@create')}}" class='btn btn-primary'>Insert Category</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=1;
                @endphp
                @foreach($categories as $category)
                <tr>
                    <td>@php echo $i++; @endphp</td>
                    <td>{{$category['name']}}</td>
                    <td>{{$category['description']}}</td>

                    <td>
                        <a href="{{action('CategoryController@edit', $category['id'])}}" class="btn btn-warning">Edit</a>&nbsp;
                        <a href="categories/{{$category['id']}}/delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>&nbsp;
                    </td>
                </tr>
                @endforeach



            </tbody>
        </table>
    </div>
@endsection
