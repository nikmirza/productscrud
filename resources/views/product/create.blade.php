@extends('layouts.app')

@section('content')

    <div class="container">
            <h2>Add New Product</h2><br/>
            <form method="post" action="{{action('ProductController@store')}}" enctype="multipart/formdata">
                @csrf
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="code">Code:</label>
                        <input type="text" class="form-control" name="code" placeholder="e.g. 10001">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="name">Name: </label>
                        <input type="text" class="form-control" name="name" placeholder="e.g. Keropok Pisang Besar">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="description">Description: </label>
                        <input type="text" class="form-control" name="description" placeholder="e.g. Keropok pisang pek 500gm">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="price">Price: (RM)</label>
                        <input type="text" class="form-control" name="price" placeholder="e.g. 10.00" >
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="brand_id">Brand: </label>
                        <select name="brand_id" class="form-control">
                            @foreach($brands as $b)
                            <option value="{{ $b->id }}">{{ $b->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="category_id">Category: </label>
                        <select name="category_id[]" class="form-control" multiple="true">
                            @foreach($categories as $c)
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4" style="margin-top:60px">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </form>
    </div>

@endsection
