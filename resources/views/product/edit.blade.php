@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Edit Product Information</h2><br />
            <form method="post" action="{{action('ProductController@update', $product->id)}}">
            @csrf
                <input name="_method" type="hidden" value="PATCH">
                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="code">Code:</label>
                                <input type="text" class="form-control" name="code" value="{{$product->code}}">
                        </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{$product->name}}">
                        </div>
                    </div>

                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="description">Description</label>
                                <input type="text" class="form-control" name="description" value="{{$product->description}}">
                        </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="price">Price: (RM)</label>
                        <input type="text" class="form-control" name="price" placeholder="e.g. 10.00" value="{{$product->price}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="brand_id">Brand: </label>
                            <select name="brand_id" defaultValue="{{$product->brand->id}}" class="form-control">
                                @foreach($brands as $b)
                                <option value="{{$b->id }}" @if($b->id==$product->brand->id) selected @endif>{{ $b->name }}</option>
                                @endforeach
                            </select>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                            <div class="form-group col-md-4">
                              <label for="category_id">Category:</label><br>
                              <select name="category_id[]" class="form-control" multiple="true">
                                  @foreach($categories as $category)
                                  <option value="{{$category->id }}" >{{ $category->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                            <div class="form-group col-md-4" style="margin-top:60px">
                                <button type="submit" class="btn btn-success" style="marginleft:38px">Update</button>
                            </div>
                            </div>
            </form>
    </div>

@endsection
