@extends('layouts.app')

@section('content')

    <div>
        @if(\Session::has('success'))
            <div class='alert alert-success'>
                <p><{{\Session::get('success')}}></p>
            </div><br>
        @endif


        <a href="{{action('ProductController@create')}}" class='btn btn-primary'>Insert product</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Price (RM)</th>
                    <th>Brand</th>
                    <th>Category</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=1;
                @endphp
                @foreach($products as $product)
                    <tr>
                        <td>@php echo $i++; @endphp</td>
                        <td>{{$product['code']}}</td>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['price']}}</td>
                        <td>{{$product->brand->name }}</td>
                        <td>
                            @foreach($product->category as $c)
                                {{$c->name}} <br>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-warning">Edit</a>&nbsp;
                            <a href="products/{{$product['id']}}/delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>&nbsp;
                        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
